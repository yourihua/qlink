//
//  CodeBarViewController.h
//  Epos
//
//  Created by Gary on 14/12/10.
//  Copyright (c) 2014年 Gary. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CodeBarViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate>
#pragma mark -扫码属性,iOS7以上
@property (strong,nonatomic)AVCaptureDevice * device;
@property (strong,nonatomic)AVCaptureDeviceInput * input;
@property (strong,nonatomic)AVCaptureMetadataOutput * output;
@property (strong,nonatomic)AVCaptureSession * session;
@property (strong,nonatomic)AVCaptureVideoPreviewLayer * preview;
@property (strong, nonatomic) IBOutlet UIImageView *line;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcLineTopToKuang;

@property(nonatomic,strong) void(^scanResultBlock)(NSString *result);

@end
