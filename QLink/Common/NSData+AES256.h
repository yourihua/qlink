//
//  NSData+AES256.h
//  QLink
//
//  Created by sansan on 15/10/15.
//  Copyright (c) 2015年 SANSAN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
