//
//  SeriesNumberView.h
//  QLink
//
//  Created by 尤日华 on 15-1-16.
//  Copyright (c) 2015年 SANSAN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeriesNumberView : UIView

@property(nonatomic,strong) void(^comfirmBlock)();
@property(nonatomic,strong) void(^scanBlock)();

@property (weak, nonatomic) IBOutlet UITextField *tfNumber;

@end
